# Simple finite difference derivatives, consuming xarray, using numba, producing numpy

Calculates derivative related values by simple finite difference methods. It consumes xarray.DataArray and stores (meta)data as numpy.ndarray. Core caluculation is done by Numba; thus fast, useful, and readable.

## Installation

```
pip install git+https://gitlab.com/kasugab3621/finitediffs.git
```
```
pip install git+https://gitlab.com/kasugab3621/finitediffs.git@develop
```

## Usage

See [example Jupyter notebook](./sample.ipynb).


<!--
https://pc.atsuhiro-me.net/entry/2019/11/26/010833
https://github.com/smartass101/xrbispectral
https://ajdawson.github.io/windspharm/latest/api/windspharm.xarray.html
-->