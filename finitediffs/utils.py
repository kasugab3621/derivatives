"""derivative tools for xarray.DataArray using Numba"""

import os, sys
import numpy as np
import xarray as xr
from numba import njit


@njit('f4[:,:](f4[:,:],f4[:,:],b1)', cache=True)
def _x2(u, dx, loop):

    dudx = np.zeros(u.shape, dtype='f4')
    dudx[:, 1:-1] = (u[:, 2:] - u[:, :-2]) / (2*dx[:, 1:-1])

    if loop:
        dudx[:, 0] = (u[:, 1] - u[:, -1]) / (2*dx[:, 0])
        dudx[:, -1] = (u[:, 0] - u[:, -2]) / (2*dx[:, -1])
    else:
        dudx[:, 0] = (u[:, 1] - u[:, 0]) / dx[:, 0]
        dudx[:, -1] = (u[:, -1] - u[:, -2]) / dx[:, -1]

    # remove polar points
    if dx[0,0]/dx[1,0] < 1e-4:
        dudx[0, :] = np.nan
    if dx[-1,0]/dx[-2,0] < 1e-4:    
        dudx[-1, :] = np.nan

    return dudx


@njit('f4[:,:,:](f4[:,:,:],f4[:,:],b1)', cache=True)
def _x3(u, dx, loop):

    dudx = np.zeros(u.shape, dtype='f4')
    dudx[:,:, 1:-1] = (u[:,:, 2:] - u[:,:, :-2]) / (2*dx[:, 1:-1])

    if loop:
        dudx[:,:, 0] = (u[:,:, 1] - u[:,:, -1]) / (2*dx[:, 0])
        dudx[:,:, -1] = (u[:,:, 0] - u[:,:, -2]) / (2*dx[:, -1])
    else:
        dudx[:,:, 0] = (u[:,:, 1] - u[:,:, 0]) / dx[:, 0]
        dudx[:,:, -1] = (u[:,:, -1] - u[:,:, -2]) / dx[:, -1]

    # remove polar points
    if dx[0,0]/dx[1,0] < 1e-4:
        dudx[:, 0, :] = np.nan
    if dx[-1,0]/dx[-2,0] < 1e-4:    
        dudx[:, -1, :] = np.nan

    return dudx


@njit('f4[:,:,:,:](f4[:,:,:,:],f4[:,:],b1)', cache=True)
def _x4(u, dx, loop):

    dudx = np.zeros(u.shape, dtype='f4')
    dudx[:,:,:, 1:-1] = (u[:,:,:, 2:] - u[:,:,:, :-2]) / (2*dx[:, 1:-1])

    if loop:
        dudx[:,:,:, 0] = (u[:,:,:, 1] - u[:,:,:, -1]) / (2*dx[:, 0])
        dudx[:,:,:, -1] = (u[:,:,:, 0] - u[:,:,:, -2]) / (2*dx[:, -1])
    else:
        dudx[:,:,:, 0] = (u[:,:,:, 1] - u[:,:,:, 0]) / dx[:, 0]
        dudx[:,:,:, -1] = (u[:,:,:, -1] - u[:,:,:, -2]) / dx[:, -1]

    # remove polar points
    if dx[0,0]/dx[1,0] < 1e-4:
        dudx[:,:, 0, :] = np.nan
    if dx[-1,0]/dx[-2,0] < 1e-4:    
        dudx[:,:, -1, :] = np.nan

    return dudx


@njit('f4[:,:,:,:](f4[:,:,:,:],f4[:,:],b1)', cache=True)
def _x4e(u, dx, loop):  # x3 slower

    dudx = np.zeros(u.shape, dtype='f4')

    for j in range(u.shape[0]):
        for i in range(u.shape[1]):

            dudx[j,i,:, 1:-1] = (u[j,i,:, 2:] - u[j,i,:, :-2]) / (2*dx[:, 1:-1])

            if loop:
                dudx[j,i,:, 0] = (u[j,i,:, 1] - u[j,i,:, -1]) / (2*dx[:, 0])
                dudx[j,i,:, -1] = (u[j,i,:, 0] - u[j,i,:, -2]) / (2*dx[:, -1])
            else:
                dudx[j,i,:, 0] = (u[j,i,:, 1] - u[j,i,:, 0]) / dx[:, 0]
                dudx[j,i,:, -1] = (u[j,i,:, -1] - u[j,i,:, -2]) / dx[:, -1]

    # remove polar points
    if dx[0,0]/dx[1,0] < 1e-4:
        dudx[:,:, 0, :] = np.nan
    if dx[-1,0]/dx[-2,0] < 1e-4:    
        dudx[:,:, -1, :] = np.nan

    return dudx


@njit('f4[:,:](f4[:,:],f4)', cache=True)
def _y2(u, dy):

    dudy = np.zeros(u.shape, dtype='f4')
    dudy[1:-1, :] = (u[2:, :] - u[:-2, :]) / (2*dy)

    dudy[0, :] = (u[1, :] - u[0, :]) / dy
    dudy[-1, :] = (u[-1, :] - u[-2, :]) / dy

    return dudy


@njit('f4[:,:,:](f4[:,:,:],f4)', cache=True)
def _y3(u, dy):

    dudy = np.zeros(u.shape, dtype='f4')
    dudy[:,1:-1, :] = (u[:,2:, :] - u[:,:-2, :]) / (2*dy)

    dudy[:,0, :] = (u[:,1, :] - u[:,0, :]) / dy
    dudy[:,-1, :] = (u[:,-1, :] - u[:,-2, :]) / dy

    return dudy


@njit('f4[:,:,:,:](f4[:,:,:,:],f4)', cache=True)
def _y4(u, dy):

    dudy = np.zeros(u.shape, dtype='f4')
    dudy[:,:,1:-1, :] = (u[:,:,2:, :] - u[:,:,:-2, :]) / (2*dy)

    dudy[:,:,0, :] = (u[:,:,1, :] - u[:,:,0, :]) / dy
    dudy[:,:,-1, :] = (u[:,:,-1, :] - u[:,:,-2, :]) / dy

    return dudy


@njit('f4[:,:,:,:](f4[:,:,:,:],f4)', cache=True)
def _y4e(u, dy):  # x3 slower

    dudy = np.zeros(u.shape, dtype='f4')

    for j in range(u.shape[0]):
        for i in range(u.shape[1]):

            dudy[j,i,1:-1, :] = (u[j,i,2:, :] - u[j,i,:-2, :]) / (2*dy)

            dudy[j,i,0, :] = (u[j,i,1, :] - u[j,i,0, :]) / dy
            dudy[j,i,-1, :] = (u[j,i,-1, :] - u[j,i,-2, :]) / dy

    return dudy


@njit('f4[:,:,:](f4[:,:,:],f4[:])', cache=True)
def _z3(u, dz):

    dz = np.expand_dims(dz, -1)
    dz = np.expand_dims(dz, -1)

    dudz = np.zeros(u.shape, dtype='f4')
    dudz[1:-1, :, :] = (u[2:, :, :] - u[:-2, :, :]) / dz[1:-1] / 2

    dudz[0, :, :] = (u[1, :, :] - u[0, :, :]) / dz[0]
    dudz[-1, :, :] = (u[-1, :, :] - u[-2, :, :]) / dz[-1]

    return dudz


@njit('f4[:,:,:,:](f4[:,:,:,:],f4[:])', cache=True)
def _z4(u, dz):

    dz = np.expand_dims(dz, -1)
    dz = np.expand_dims(dz, -1)

    dudz = np.zeros(u.shape, dtype='f4')
    dudz[:, 1:-1, :, :] = (u[:, 2:, :, :] - u[:,:-2, :, :]) / dz[1:-1] / 2
    dudz[:, 0, :, :] = (u[:, 1, :, :] - u[:, 0, :, :]) / dz[0]
    dudz[:, -1, :, :] = (u[:, -1, :, :] - u[:, -2, :, :]) / dz[-1]

    return dudz


@njit('f4[:,:,:](f4[:,:,:],f4[:,:,:])', cache=True)
def _z32(u, z):

    dudz = np.zeros(u.shape, dtype='f4')
    dudz[1:-1, :, :] = (u[2:, :, :] - u[:-2, :, :]) / (z[2:, :, :] - z[:-2, :, :]) / 2

    dudz[0, :, :] = (u[1, :, :] - u[0, :, :]) / (z[1, :, :] - z[0, :, :])
    dudz[-1, :, :] = (u[-1, :, :] - u[-2, :, :]) / (u[-1, :, :] - u[-2, :, :])

    return dudz


@njit('f4[:,:,:,:](f4[:,:,:,:],f4[:,:,:,:])', cache=True)
def _z42(u, z):

    dudz = np.zeros(u.shape, dtype='f4')
    dudz[:, 1:-1, :, :] = (u[:, 2:, :, :] - u[:,:-2, :, :]) /  (z[:, 2:, :, :] - z[:,:-2, :, :]) / 2
    dudz[:, 0, :, :] = (u[:, 1, :, :] - u[:, 0, :, :]) /  (z[:, 1, :, :] - z[:, 0, :, :])
    dudz[:, -1, :, :] = (u[:, -1, :, :] - u[:, -2, :, :]) / (z[:, -1, :, :] - z[:, -2, :, :])

    return dudz


#@njit('f4[:,:,:](f4[:,:,:],f4)', cache=True)
@njit(cache=True)
def _t321(u, dt):

    dudt = np.zeros(u.shape, dtype='f4')
    dudt[1:-1,...] = (u[2:,...] - u[:-2,...]) / (2*dt)

    dudt[0,...] = (u[1,...] - u[0,...]) / dt
    dudt[-1,...] = (u[-1,...] - u[-2,...]) / dt

    return dudt


@njit('f4[:,:,:,:](f4[:,:,:,:],f4)', cache=True)
def _t4(u, dt):

    dudt = np.zeros(u.shape, dtype='f4')
    dudt[1:-1] = (u[2:] - u[:-2]) / (2*dt)

    dudt[0] = (u[1] - u[0]) / dt
    dudt[-1] = (u[-1] - u[-2]) / dt

    return dudt


class Extractor():

    def extract(self, *args, z=False, noload=False):
        R = 6371.2*1e3
        omg = 7.292*1e-5

        if len(args) == 2:
            u, v = args
            if not isinstance(u, xr.DataArray) or not isinstance(v, xr.DataArray):
                raise TypeError('u and v must be xr.DataArray')
            self.uv = True
        elif len(args) == 1:
            u = args[0]
            if not isinstance(u, xr.DataArray):
                raise TypeError('array must be xr.DataArray')
            self.uv = False
        else:
            raise ValueError('The args must have 1 or 2 DataArray(s)')

        if not u.ndim in [2, 3, 4]:
            raise ValueError(f'DataArray must have 2, 3, or 4 dimensions, but {self.ndim}')

        if isinstance(z, xr.DataArray):
            self.z2 = z
            self.z2_flag = True
        else:
            self.z2_flag = False
 
        self.shape = u.shape
        self.coords = u.coords
        self.dims = u.dims
        self.ndim = u.ndim
        
        try:
            d4 = u[u.dims[-4]].values
            f4 = True
        except:
            f4 = False
        try:
            d3= u[u.dims[-3]].values
            f3 = True
        except:
            f3 = False
            times = None
            levs = None

        if f4:
            times = d4
            levs = d3.astype('f4')
        
        elif f3:
            if type(d3[0]) == np.datetime64:
                times = d3
                levs = None
            else:
                times = None
                levs = d3.astype('f4')

        lats = u[u.dims[-2]].values.astype('f4')
        lons = u[u.dims[-1]].values.astype('f4')

        self.lons = lons
        self.lats = lats
        self.levs = levs
        self.times = times

        dlon = lons[1] - lons[0]
        dlat = lats[1] - lats[0]  # could be minus

        try:
            iz = len(levs)
        except:
            iz = 1

        if iz > 3:
            dlev = np.zeros((len(levs)), dtype='f4')
            dlev[1:-1] = (levs[2:] - levs[:-2]) / 2
            dlev[0] = (levs[1] - levs[0])
            dlev[-1] = (levs[-1] - levs[-2])
        else:
            dlev = None

        try:
            it = len(times)
        except:
            it = 1

        if it >= 3:
            dt = times[1] - times[0]
            dt = dt.astype('f4')*1e-9  # nano sec -> seconds
        else:
            dt = None

        _, lats2 = np.meshgrid(lons, lats)

        # store paramters
        self.R = R  # m 
        self.omg = omg  # /s
        self.dlon = dlon
        self.dlat = dlat
        self.dlev = dlev
        self._dz = dlev
        self._dt = dt
        self.lats2 = lats2
        # coriolis parameter
        self.f = (2 * omg * np.sin(np.deg2rad(lats2))).astype('f4')

        # make denominator meters
        self._dx = 2 * np.pi * R * np.cos(np.deg2rad(lats2)) * dlon / 360.
        self._dy = 2 * np.pi * R * dlat / 360.

        # lon loop check
        if lons[0] == lons[-1] + dlon or lons[0] == lons[-1] + dlon + 360. or lons[0] == lons[-1] + dlon - 360.:
            self.loop = True
        else:
            self.loop = False

        self.xru = u
        if self.uv:  self.xrv = v

        if not noload:
            self.load()

    def load(self):
        self.u = self.xru.values.astype('f4')
        if self.uv:  self.v = self.xrv.values.astype('f4')            


class NumbaCaller():
    """
    wrapper for defining dimension and call Numba-based modules
    abstract class
    """

    def check_load(self):
        if not hasattr(self, 'u'):
            raise ValueError('data is not loaded, use .load() first')

    def check_uv(self):
        if not self.uv:  raise ValueError('v missing')

    def _x(self, u):
        if self.ndim == 4:
            return _x4(u, self._dx, self.loop)
        elif self.ndim == 3:
            return _x3(u, self._dx, self.loop)
        elif self.ndim == 2:
            return _x2(u, self._dx, self.loop)

    def _y(self, u):
        if self.ndim == 4:
            return _y4(u, self._dy)
        elif self.ndim == 3:
            return _y3(u, self._dy)
        elif self.ndim == 2:
            return _y2(u, self._dy)

    def _z(self, u):  # pressure derivative
        if self.ndim == 4:
            return _z4(u, self._dz)
        elif self.ndim == 3:
            return _z3(u, self._dz)
        else:
            raise ValueError('DaraArray must have 3D or 4D')

    def _z2(self, u, z):  # height derivative 
        if self.ndim == 4:
            return _z42(u, z)
        elif self.ndim == 3:
            return _z32(u, z)
        else:
            raise ValueError('DaraArray must have 3D or 4D')

    def _t(self, u):
        if self.ndim == 4:
            return _t4(u, self._dt)
        elif self._dt is not None:
            return _t321(u, self._dt)
        else:
            raise ValueError('To get temporal derivative, DaraArray must have time coordinate in the highest dimension')


class D(Extractor, NumbaCaller):

    def __init__(self, u, z=False, noload=False):
        """
        Derivative calcuator for scalar (kasuga, 2023/08/01)
        Extract (meta) data from xarray.DataAttay,
        convert to numpy.ndarray and calc. with Numba

        Note:
        -----
        DataArray can be 4/3/2 dimensions and its coords must be python-ordinal order,
        da.dims = ['time', 'level', 'latitude', 'longitude']

        By default, data will be loaded imediately.
        If noload=True, the loading will be passed, then load by .load() methods
        """
        self.extract(u, z=z)

    def values(self):
        return self.u

    def dx(self):
        self.check_load()
        return self._x(self.u)

    def dy(self):
        self.check_load()
        return self._y(self.u)

    #def dz(self):
    def dp(self):
        self.check_load()
        return self._z(self.u)

    def dz(self):
        self.check_load()
        self.check_z()
        return self._z2(self.u, self.z2)

    def check_z():
        if not self.z2_flag:
            raise ValueError('No geopotential heihgt DataArray is specified')

    def dt(self):
        self.check_load()
        return self._t(self.u)

    def gradient(self):
        self.check_load()
        return self._x(self.u), self._y(self.u)

    def laplacian(self):
        self.check_load()
        ux = self._x(self.u)
        uy = self._y(self.u)
        return self._x(self.ux) + self._y(self.uy)

    def beta(self):
        #return self._y(self.f)
        return _y2(self.f, self._dy)

    def __str__(self):
        print(self.xru)
        return ''


class UV(Extractor, NumbaCaller):

    def __init__(self, u, v, noload=False):
        """
        Derivative calcuator for vector (kasuga, 2023/08/01)
        Extract (meta) data from xarray.DataAttay,
        convert to numpy.ndarray and calc. with Numba

        Note:
        -----
        DataArray can be 4/3/2 dimensions and its coords must be python-ordinal order,
        da.dims = ['time', 'level', 'latitude', 'longitude']

        By default, data will be loaded imediately.
        If noload=True, the loading will be passed, then load by .load() methods
        """
        self.extract(u, v, noload=noload)

    def divergence(self):
        self.check_uv()
        self.check_load()
        return self._x(self.u) + self._y(self.v)

    def vorticity(self):
        self.check_uv()
        self.check_load()
        return self._x(self.v) - self._y(self.u)

    def absolute_vorticity(self):
        return self.vorticity() + self.f

    def hdivg(self):
        return self.divergence()

    def hcurl(self):
        return self.vorticity()

    def gradient_u(self):
        self.check_uv()
        self.check_load()
        return self._x(self.u), self._y(self.u)

    def gradient_v(self):
        self.check_uv()
        self.check_load()
        return self._x(self.v), self._y(self.v)

    def beta(self):
        #return self._y(self.f)
        return _y2(self.f, self._dy)

    def __str__(self):
        print(self.xru)
        print('')
        print(self.xrv)
        return ''
