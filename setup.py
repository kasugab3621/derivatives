import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="finitediffs",
    version="0.3.2",
    author="Satoru Kasuga",
    author_email="kasugab3621@outlook.com",
    description="Simple finite difference derivatives, consuming xarray, using numba, producing numpy",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/kasugab3621/finitediffs.git",
    packages=setuptools.find_packages(),
    install_requires=['xarray', 'numba'],
)
