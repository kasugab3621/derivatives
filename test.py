#!/usr/bin/env python3

import time

import xarray as xr
#ds = xr.tutorial.load_dataset('eraint_uvz').sel(month=1, level=500).squeeze()
#ds = ds.sel(longitude=slice(150, 190), latitude=slice(80,10))
#u = ds.u
#v = ds.v
'''
z = xr.open_dataarray('../z.nc').isel(longitude=slice(5), latitude=slice(5), level=slice(5), time=5)

from finitediffs import D
a = D(z)
dz = a.dy()
dz = a.dx()
quit()
'''
u = xr.open_dataarray('../u.nc')
v = xr.open_dataarray('../v.nc')

u =u.isel(time=1,level=10).squeeze()
v =v.isel(time=1,level=10).squeeze()

from finitediffs import D, UV

time_start = time.time()

a = UV(u, v)
vor_f = a.vorticity()

time_end = time.time()
time_f = time_end - time_start

print('time_f: ', time_f)
print(a)
quit()

import metpy.calc as mc
from metpy.units import units

time_start = time.time()

vor_m = mc.vorticity(u * units('m/s'), v * units('m/s'))

time_end = time.time()
time_m = time_end - time_start


print('time_f: ', time_f)
print('time_m: ', time_m)


#quit()

vor_f = vor_f*units('1/second')


square_error = (vor_m-vor_f)**2
rmse = square_error.mean(dim='latitude').mean(dim='longitude')**(.5)
print(rmse)


import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
ax = plt.axes(projection=ccrs.Orthographic(135, 50))
ax.coastlines()
ax.set_extent([130,150,30,50])
r = 0.0001
#s = ax.contourf(a.lons, a.lats, vor_m-vor_f, np.arange(-r,r,r/10),
s = ax.contourf(a.lons, a.lats, vor_m, np.arange(-r,r,r/10),
                transform=ccrs.PlateCarree())
plt.colorbar(s)
plt.savefig('hogemv.png')

